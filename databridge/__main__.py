import argparse

parser = argparse.ArgumentParser()

parser.add_argument('task', help='which task to perform', choices=(
    'run_stimulus_server','run_analysis','run_visualization_server'))
parser.add_argument('-s', '--settings', help='path to the settings file to use')
parser.add_argument('-o', '--host', help='which host to run the server as')
parser.add_argument('-p', '--port', help='which port to run the server on')
parser.add_argument('-i', '--includes', help='additional files to load', nargs='*')

args = parser.parse_args()

if args.task == 'run_stimulus_server':
    print('starting stimulus server')
elif args.task == 'run_analysis':
    print('starting analysis')
elif args.task == 'run_visualization_server':
    print('starting visualization server')
else:
    print('string "{}" not recognized'.format(args.task))
