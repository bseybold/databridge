# These are the default settings for an application or server

# Environment Settings
definition_files = []

# Server Settings
server_host_stimuli = "127.0.0.1"
server_port_stimuli = 8000

server_host_visualize = "127.0.0.1"
server_port_visualize = 8000

static_dir = '/'
app_dir = '/'
static_root = 'static/'
debug = True
reloader = True
