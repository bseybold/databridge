from distutils.core import setup

setup (
    name = 'databridge',
    packages = ['databridge'],
    version = '0.1',
    description = 'Tools to connect data to visualization. Targeted to the wide, scientific community.',
    author = 'Bryan Seybold',
    author_email = 'baseybold@gmail.com',
    url = 'http://',
    download_url = 'http://',
    keywords = ['science','data processing','data formats','data visualization','data','visualization'],
    classifiers = ['Programming Language :: Python :: 2.7'],
    long_description = '''
    ''')
